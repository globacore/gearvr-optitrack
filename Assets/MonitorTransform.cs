﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MonitorTransform : MonoBehaviour {

    public Transform target;
    private Text text;

	// Use this for initialization
	void Start () {
        text = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "X: " + target.transform.localPosition.x + "\n" + "Y: " + target.transform.localPosition.y + "\n" + "Z: " + target.transform.localPosition.z;
    }
}
