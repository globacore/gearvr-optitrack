﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OptiTrackManagerOld : MonoBehaviour {

    public Transform target;
    public Camera cam;
    public Text debugText;

    private bool haveSample;
    private Vector3 prevPos;

	// Use this for initialization
	void Start () {
        OSCHandler.Instance.Init();
        haveSample = false;
	}
	
	// Update is called once per frame
	void Update () {
        OSCHandler.Instance.UpdateLogs();

        Dictionary<string, ServerLog> servers = new Dictionary<string, ServerLog>();
        servers = OSCHandler.Instance.Servers;

        ServerLog optiTrack = servers["OptiTrack"];

        if (optiTrack.server.LastReceivedPacket != null && !haveSample)
        {
            prevPos.x = (float)(optiTrack.server.LastReceivedPacket.Data[2]);
            prevPos.y = (float)(optiTrack.server.LastReceivedPacket.Data[3]);
            prevPos.z = -(float)(optiTrack.server.LastReceivedPacket.Data[4]);

            haveSample = true;
        }
        else if (optiTrack.server.LastReceivedPacket != null && haveSample)
        {
            Vector3 newPos;
            newPos.x = (float)(optiTrack.server.LastReceivedPacket.Data[2]);
            newPos.y = (float)(optiTrack.server.LastReceivedPacket.Data[3]);
            newPos.z = -(float)(optiTrack.server.LastReceivedPacket.Data[4]);

            if (debugText)
                debugText.text = "X: " + newPos.x + "\nY: " + newPos.y + "\nZ: " + newPos.z;

            Vector3 delta = newPos - prevPos;

            target.position += delta;

            prevPos = newPos;

        }
        //else
           // Debug.Log("No Packet");
    }
}
