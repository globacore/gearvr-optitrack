﻿/**
 * Adapted from johny3212
 * Written by Matt Oskamp
 */
using UnityEngine;
using System.Collections;

public class OptiTrackObject : MonoBehaviour {

    public bool useRotation;
    public Camera cam;
	public int rigidbodyIndex;
	public Vector3 rotationOffset;
    private Vector3 prevPos;
    private bool haveSample;

    // Use this for initialization
    void Start () {
        haveSample = false;

        OVRTouchpad.Create();
        OVRTouchpad.TouchHandler += HandleTouchHandler;
    }
	
	// Update is called once per frame
	void Update () {
		Vector3 newPos = OptiTrackVRManager.Instance.getPosition(rigidbodyIndex);

        this.transform.localPosition = newPos;
        
        if (useRotation)
        {
            Quaternion rot = OptiTrackVRManager.Instance.getOrientation(rigidbodyIndex);
            rot = rot * Quaternion.Euler(rotationOffset);

            this.transform.rotation = rot;
        }

        prevPos = newPos;
    }
    
    public void UpdateOffset()
    {
        Debug.Log("Tracked Rotation: " + OptiTrackVRManager.Instance.getOrientation(rigidbodyIndex).eulerAngles);
        this.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, ((OptiTrackVRManager.Instance.getOrientation(rigidbodyIndex).eulerAngles) - rotationOffset).y, 0.0f));
    }

    void HandleTouchHandler(object sender, System.EventArgs e)
    {
        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap)
        {
            this.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, -cam.transform.localRotation.eulerAngles.y, 0.0f));
        }
    }
}
