﻿/**
 * Adapted from johny3212
 * Written by Matt Oskamp
 */
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;
using OptitrackManagement;

public class OptiTrackVRManager : MonoBehaviour 
{
	public string myName;
	public float scale = 20.0f;
	private static OptiTrackVRManager instance;
    public OptiTrackObject player;
    public Image overlay;

	public Vector3 origin = Vector3.zero; // set this to wherever you want the center to be in your scene

	public static OptiTrackVRManager Instance
	{
		get { return instance; } 
	}

	void Awake()
	{
        overlay.color = new Color(0.0f, 0.0f, 0.0f, 1.0f);
		instance = this;
	}

	~OptiTrackVRManager()
	{      
		Debug.Log("OptitrackManager: Destruct");
        //OptitrackManagement.DirectMulticastSocketClient.Close();
        OptitrackManagement.DirectUnicastSocket.Close();
    }
	
	void Start () 
	{
		Debug.Log(myName + ": Initializing");

        //OptitrackManagement.DirectMulticastSocketClient.Start();
        OptitrackManagement.DirectUnicastSocket.Start();
        Application.runInBackground = true;
        StartCoroutine(SetPlayerRotation());
	}

    //automatic set player position, need to ensure that the headset is tracked
    private IEnumerator SetPlayerRotation()
    {
        yield return new WaitForSeconds(0.5f);

        while (!OptitrackManagement.DirectUnicastSocket.HasSample())
        {
            yield return new WaitForSeconds(0.1f);
        }
        Debug.Log("Optirack VR Update Player Rotation Offset");
        player.UpdateOffset();
        overlay.CrossFadeAlpha(0.0f, 0.5f, false);
    }

	public OptiTrackRigidBody getOptiTrackRigidBody(int index)
	{
		// only do this if you want the raw data
		if(OptitrackManagement.DirectUnicastSocket.IsInit())
		{
			DataStream networkData = OptitrackManagement.DirectUnicastSocket.GetDataStream();
			return networkData.getRigidbody(index);
		}
		else
		{
			OptitrackManagement.DirectUnicastSocket.Start();
			return getOptiTrackRigidBody(index);
		}
	}

	public Vector3 getPosition(int rigidbodyIndex)
	{
		if(OptitrackManagement.DirectUnicastSocket.IsInit())
		{
			DataStream networkData = OptitrackManagement.DirectUnicastSocket.GetDataStream();
			Vector3 pos = origin + networkData.getRigidbody(rigidbodyIndex).position * scale;
			//pos.x = pos.x; // not really sure if this is the best way to do it
			//pos.y = pos.y; // these may change depending on your configuration and calibration
			pos.z = -pos.z;
			return pos;
		}
		else
		{
			return Vector3.zero;
		}
	}

	public Quaternion getOrientation(int rigidbodyIndex)
	{
		// should add a way to filter it
		if(OptitrackManagement.DirectUnicastSocket.IsInit())
		{
			DataStream networkData = OptitrackManagement.DirectUnicastSocket.GetDataStream();
			Quaternion rot = networkData.getRigidbody(rigidbodyIndex).orientation;

			// change the handedness from motive
			//rot = new Quaternion(rot.z, rot.y, rot.x, rot.w); // depending on calibration
			
			// Invert pitch and yaw
			Vector3 euler = rot.eulerAngles;
			rot.eulerAngles = new Vector3(euler.x, -euler.y, euler.z); // these may change depending on your calibration

			return rot;
		}
		else
		{
			return Quaternion.identity;
		}
	}

	public void DeInitialize()
	{
		//OptitrackManagement.DirectUnicastSocket.Close();
	}

    public void OnApplicationQuit()
    {
        Debug.Log("CLEAN UP SOCKET");
        DeInitialize();
    }

	// Update is called once per frame
	void Update () 
	{
        /*if (recieving)
            OptitrackManagement.DirectUnicastSocket.ReceiveSync();

        if (Input.GetKeyDown(KeyCode.R))
        {
            recieving = !recieving;
        }*/
    }
}